-- --------------------------------------------------------
-- 主机:                           dbserver
-- 服务器版本:                        5.5.19 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 base 的数据库结构
CREATE DATABASE IF NOT EXISTS `base` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `base`;


-- 导出  表 base.sys_application 结构
CREATE TABLE IF NOT EXISTS `sys_application` (
  `id` bigint(20) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  `creator` varchar(30) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updater` varchar(30) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `appCode` varchar(20) DEFAULT NULL,
  `appContext` varchar(20) DEFAULT NULL,
  `appHost` varchar(20) DEFAULT NULL,
  `appIcon` varchar(20) DEFAULT NULL,
  `appKey` varchar(60) DEFAULT NULL,
  `appName` varchar(20) DEFAULT NULL,
  `appPort` varchar(20) DEFAULT NULL,
  `appStatus` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5lmuivdctxq2cacx1wqigk5bt` (`appCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 base.sys_organization 结构
CREATE TABLE IF NOT EXISTS `sys_organization` (
  `id` bigint(20) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  `creator` varchar(30) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updater` varchar(30) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `orgCode` varchar(20) DEFAULT NULL,
  `orgIcon` varchar(20) DEFAULT NULL,
  `orgName` varchar(50) DEFAULT NULL,
  `orgPid` varchar(40) DEFAULT NULL,
  `orgSeq` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_qthi5iyk7otvfvc0nov9l1mjd` (`orgCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 base.sys_resource 结构
CREATE TABLE IF NOT EXISTS `sys_resource` (
  `id` bigint(20) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  `creator` varchar(30) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updater` varchar(30) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `resCode` varchar(20) DEFAULT NULL,
  `resIcon` varchar(20) DEFAULT NULL,
  `resName` varchar(20) DEFAULT NULL,
  `resPath` varchar(64) DEFAULT NULL,
  `resPid` varchar(40) DEFAULT NULL,
  `resSeq` int(11) DEFAULT NULL,
  `resType` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_f9rhox0u67eawc2ds2ccpg6cf` (`resCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 base.sys_role 结构
CREATE TABLE IF NOT EXISTS `sys_role` (
  `id` bigint(20) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  `creator` varchar(30) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updater` varchar(30) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `roleCode` varchar(20) DEFAULT NULL,
  `roleIcon` varchar(20) DEFAULT NULL,
  `roleName` varchar(20) DEFAULT NULL,
  `roleSeq` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 base.sys_role_resource 结构
CREATE TABLE IF NOT EXISTS `sys_role_resource` (
  `id` bigint(20) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  `creator` varchar(30) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updater` varchar(30) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `appId` varchar(40) DEFAULT NULL,
  `resId` varchar(40) DEFAULT NULL,
  `roleId` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 base.sys_test 结构
CREATE TABLE IF NOT EXISTS `sys_test` (
  `id` bigint(20) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  `creator` varchar(30) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updater` varchar(30) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `testName` varchar(50) DEFAULT NULL,
  `testTime` bigint(20) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 base.sys_user 结构
CREATE TABLE IF NOT EXISTS `sys_user` (
  `id` bigint(20) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  `creator` varchar(30) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updater` varchar(30) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `email` varchar(11) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `orgId` varchar(40) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `salt` varchar(20) DEFAULT NULL,
  `truename` varchar(11) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_51bvuyvihefoh4kp5syh2jpi4` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 base.sys_user_resource 结构
CREATE TABLE IF NOT EXISTS `sys_user_resource` (
  `id` bigint(20) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  `creator` varchar(30) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updater` varchar(30) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `appId` varchar(40) DEFAULT NULL,
  `resId` varchar(40) DEFAULT NULL,
  `userId` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
